import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = () => new Vuex.Store({
  state: {
    enableNightMode: false,
    loaded: false,
    is_auth: false,
    categories: [],
    current_user: {},
    popular_posts: [],
    token: ""
  },
  mutations: {
    setCategoriesList(state, category_list) {
      state.categories = category_list;
    },
    toggleNightMode(state) {
      state.enableNightMode = !state.enableNightMode;
    },
    setNightMode(state, value) {
      state.enableNightMode = value;
    },
    setLoaded(state, value) {
      state.loaded = value;
    },
    setAuth(state, auth) {
      state.is_auth = auth;
    },
    setCurrentUser(state, user) {
      state.current_user = user;
    },
    setPopularPosts(state, posts) {
      state.popular_posts = posts;
    },
    setToken(state, token) {
      state.token = token;
    },
    setAllNotificationsAsRead(state) {
      state.current_user.count_unread_notification = 0;
    }
  },
  getters: {
    filterCategories: state => str => {
      return state.categories.filter(category => category.toLowerCase().indexOf(str.toLowerCase()) > -1)
    }
  },
  actions: {
    async api_call (context, group, method, data) {
    }
  }
});


export default store;
