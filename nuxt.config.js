import config from './config'

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Jeembo - мемы, комиксы, видео',
    meta: [
      {charset: 'utf-8'},
      {name: "msapplication-TileColor", content: "#202020"},
      {name: "msapplication-TileImage", content: "/favicon/ms-icon-144x144.png"},
      {name: "theme-color", content: "202020"},
      {name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'},
      {
        hid: 'description',
        name: 'description',
        content: 'Jeembo - это уникальные мемы, комиксы и видео. Общайтесь и наслаждайтесь отличным юмором и уникальным контентом!'
      }
    ],
    link: [
      {type: "image/png", rel: "apple-touch-icon", sizes: "57x57", href: '/favicon/apple-icon-57x57.png'},
      {type: "image/png", rel: "apple-touch-icon", sizes: "72x72", href: '/favicon/apple-icon-57x57.png'},
      {type: "image/png", rel: "apple-touch-icon", sizes: "76x76", href: '/favicon/apple-icon-76x76.png'},
      {type: "image/png", rel: "apple-touch-icon", sizes: "114x114", href: '/favicon/apple-icon-114x114.png'},
      {type: "image/png", rel: "apple-touch-icon", sizes: "120x120", href: '/favicon/apple-icon-120x120.png'},
      {type: "image/png", rel: "apple-touch-icon", sizes: "144x144", href: '/favicon/apple-icon-144x144.png'},
      {type: "image/png", rel: "apple-touch-icon", sizes: "152x152", href: '/favicon/apple-icon-152x152.png'},
      {type: "image/png", rel: "apple-touch-icon", sizes: "180x180", href: '/favicon/apple-icon-180x180.png'},
      {rel: 'icon', type: "image/png", sizes: "72x72", href: '/favicon/apple-icon-72x72.png'},
      {rel: 'icon', type: "image/png", sizes: "192x192", href: '/favicon/android-icon-192x192.png'},
      {rel: 'icon', type: "image/png", sizes: "32x32", href: '/favicon/favicon-32x32.png'},
      {rel: 'icon', type: "image/png", sizes: "96x96", href: '/favicon/favicon-96x96.png'},
      {rel: 'icon', type: "image/png", sizes: "96x96", href: '/favicon/favicon-16x16.png'},
      {rel: 'icon', type: "image/png", href: '/favicon/favicon-16x16.png'},
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: 'manifest', type: 'image/x-icon', href: '/favicon/manifest.json'},
      {rel: 'stylesheet', href: "https://fonts.googleapis.com/css?family=Open+Sans"},
      {rel: 'stylesheet', href: "/bootstrap.min.css"},
      {rel: 'stylesheet', href: "/fonts/podkova.css"},
      {
        rel: 'stylesheet',
        href: 'https://use.fontawesome.com/releases/v5.6.1/css/all.css',
        integrity: "sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP",
        crossorigin: 'anonymous'
      }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: {color: '#3B8070'},
  plugins: ['~plugins', "~plugins/JeemboAPI"],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, {isDev, isClient}) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
  },

  fontawesome: {
    component: 'fa'
  },
  sitemap: {
    generate: true,
    hostname: 'https://jeembo.org'
  },
  modules: [
    ['bootstrap-vue/nuxt', {css: false}],
    '@nuxtjs/font-awesome',
  ],
  server: {
    port: 3000, // default: 3000
    host: "0.0.0.0", // default: localhost
  },
};
