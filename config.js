export default {
  prod: {
    "API_URL": "http://api.jeembo.org",
    "SERVER_URL": "localhost"
  },
  dev: {
    "API_URL": "http://192.168.0.102:3000",
    "SERVER_URL": "192.168.0.102"
  }
}
