export default {
  data() {
  },

  methods: {
  },
  beforeMount() {
    const isAuth = this.$store.state.current_user.id;
    if(!isAuth) {
      this.$router.push({
        "path": "/"
      })
    }
  }
}
