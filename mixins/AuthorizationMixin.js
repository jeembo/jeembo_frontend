export default {
  data() {
    return {
      is_authenticated: false,
      token: undefined,
    }
  },

  methods: {
    setCredentials(token, user) {
      localStorage.setItem("token", token);
      this.is_authenticated = true;
      this.user = user;
    },
  },
  beforeMount() {
  }
}
