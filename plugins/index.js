import Vue from 'vue'
import JeemboEditor from './JeemboEditor'
import JeemboAPI from './JeemboAPI'
import JeemboInfinityScroll from './JeemboInfinityScroll'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
import VueViewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'

moment.locale("ru");

Vue.use(VueMoment, {
  moment,
});

Vue.use(JeemboEditor);
Vue.use(JeemboInfinityScroll);
Vue.use(VueViewer);
Vue.use(JeemboAPI, {
  baseUrl: "https://api.jeembo.org"
});
