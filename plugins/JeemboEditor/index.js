import FreckleEditor from './components/JeemboEditor'

export default {
    install(Vue, options) {
        Vue.component('jeembo-editor', FreckleEditor)
    }
}
