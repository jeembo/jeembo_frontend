export default {
    methods: {
        onActionSelect(action) {
            this.$emit(action, this.block);
        }
    }
}