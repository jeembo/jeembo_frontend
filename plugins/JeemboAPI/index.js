import axios from "axios";

import Posts from './posts'
import Users from './users'
import Marks from "./marks";
import Comments from "./commens";
import Files from "./files";
import Notifications from "./notifications";

export class JeemboAPI {
  constructor(base_url) {
    this.baseUrl = base_url;
  }

  setToken(token) {
    localStorage.setItem("access_token", token);
  }

  delToken() {
    localStorage.removeItem("access_token");
  }

  getToken() {
    return localStorage.getItem('access_token');
  }

  request(token) {
    token = token || this.getToken();

    const headers = {};
    if (token) {
      headers['Authorization'] = `Token ${token}`;
    }
    const custom_axios = axios.create({
      baseURL: this.baseUrl,
      headers: headers
    });
    return {
      posts: new Posts(custom_axios),
      users: new Users(custom_axios),
      marks: new Marks(custom_axios),
      comments: new Comments(custom_axios),
      files: new Files(custom_axios),
      notifications: new Notifications(custom_axios)
    }
  }
}


export default {
  install(Vue, options) {
    Vue.prototype.$jeembo_api = new JeemboAPI(options['baseUrl']);
  }
}
