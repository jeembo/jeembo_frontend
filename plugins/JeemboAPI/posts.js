class Posts {
  constructor(axios) {
    this.axios = axios
  }

  list(page, feed, category, author_id) {
    const params = {
      page: page ? page : 1,
      feed: feed ? feed : 'popular',
      category: category ? category : 'all'
    };

    if (author_id) {
      params['author_id'] = author_id
    }

    console.log(params);
    return this.axios.get('posts', {
      params
    })
  }

  get(id) {
    return this.axios.get(`posts/${id}/`)
  }

  create(title, body, tags, anon) {
    return this.axios.post('posts/', {
      title: title,
      body: body,
      tags: tags,
      anon: anon
    })
  }
}

export default Posts
