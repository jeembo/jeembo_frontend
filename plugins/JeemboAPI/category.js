class Category {
  constructor(axios) {
    this.axios = axios
  }

  list() {
    return this.axios.get('category/')
  }
}

export default Category;
