class Notifications {
  constructor(axios) {
    this.axios = axios
  }

  get(page) {
    const params = {
      page: page ? page : 1
    };
    return this.axios.get('notifications/', {
      params
    })
  }
}

export default Notifications;
