class Files {
  constructor(axios) {
    this.axios = axios
  }

  upload(formData) {
    return this.axios.put('files/upload/post_image/', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }


  uploadAvatar(formData) {
    return this.axios.put('files/upload/avatar/', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }
}

export default Files;
