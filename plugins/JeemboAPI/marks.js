class Marks {
  constructor(axios) {
    this.axios = axios
  }

  add(type, item_id, mark) {
    return this.axios.post('marks/', {
      entity_type: type,
      item_id: item_id,
      mark: mark
    })
  }

  list(page_num) {
    return this.axios.get('marks/', {
      params: {
        page: page_num ? page_num: 1
      }
    })
  }
}

export default Marks;
