class Users {
  constructor(axios) {
    this.axios = axios;
  }


  update(data) {
    return this.axios.post('account/update/', data)
  }

  login(login, password) {
    return this.axios.post('account/login/', {
      login,
      password
    })
  }

  reg(username, email, password) {
    return this.axios.post('account/reg/', {
      username,
      password,
      email
    })
  }

  restorePassword(email) {
    return this.axios.post("account/restore_password/", {
      email
    })
  }

  getInfoCurrentUser() {
    return this.axios.get('account/get')
  }

  getUserInfo(user_id) {
    return this.axios.get(`users/${user_id}/`)
  }

  changePassword(old_password, new_password) {
    return this.axios.post('account/change_password/', {
      old_password,
      new_password
    })
  }
}

export default Users
