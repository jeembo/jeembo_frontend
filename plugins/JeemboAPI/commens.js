class Comments {
  constructor(axios) {
    this.axios = axios
  }

  list(post_id) {
    return this.axios.get(`posts/${post_id}/comments/`)
  }

  create(text, post_id, parent_comment_id) {
    return this.axios.post(`posts/${post_id}/comments/`, {
      "text": text,
      "parent_comment_id": parent_comment_id
    })
  }
}

export default Comments
