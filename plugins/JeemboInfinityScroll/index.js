export default {
  install(Vue, options) {
    Vue.directive('scroll', {
      inserted: function (el, binding) {
        let f = function (evt) {
          const maxScrollPosition = document.documentElement.offsetHeight - document.documentElement.clientHeight - 200;
          console.log(maxScrollPosition);
          const currentScrollPosition = document.documentElement.scrollTop;
          // console.log("--------");
          // console.log("document.body.scrollHeight", document.body.scrollHeight);
          // console.log("document.body.offsetHeight", document.body.offsetHeight);
          // console.log("document.body.clientHeight", document.body.clientHeight);
          //
          // console.log("document.documentElement.scrollHeight", document.documentElement.scrollHeight);
          // console.log("document.documentElement.offsetHeight", document.documentElement.offsetHeight);
          // console.log("document.documentElement.clientHeight", document.documentElement.clientHeight);
          // console.log("document.documentElement.scrollTop", document.documentElement.scrollTop);

          if (currentScrollPosition >= maxScrollPosition) {
            if (binding.value(evt, el)) {
              window.removeEventListener('scroll', f)
            }
          }
        };
        window.addEventListener('scroll', f)
      }
    });


    Vue.directive('click-outside', {
      bind: function (el, binding, vnode) {
        el.clickOutsideEvent = function (event) {
          // here I check that click was outside the el and his childrens
          if (!(el == event.target || el.contains(event.target))) {
            // and if it did, call method provided in attribute value
            vnode.context[binding.expression](event);
          }
        };
        document.body.addEventListener('click', el.clickOutsideEvent)
      },
      unbind: function (el) {
        document.body.removeEventListener('click', el.clickOutsideEvent)
      },
    });
  }
}
